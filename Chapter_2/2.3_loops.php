<?php $title = '2.3 Loops'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.3_loops.php', true); }

$cars = array('Volvo', 'BMW', 'Toyota');	
	$associative_array_cars = array
	(
		'Nissan' => 'Pathfinder',
		'Honda' => 'Accord'	
	);
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>
<div class="col-lg-6">
  <table class="table table-striped">
    <h2>For Loop</h2><?php
		// a standard use of the for loop is to use the count() function to control the number of loops
        for ($i = 0; $i < count($cars) ; $i++ ) {
            $car = $cars[$i];
            print "<tr><td>$car</td></tr>";          
        }
       ?>
  </table>
</div>
<div class="col-lg-6">
  <h2>While Loop</h2>
  <table class="table table-striped"><?php
      $i = 0;
        while ($i < count($cars)) {
            $car = $cars[$i];
            print "<tr><td>$car</td></tr>";
            $i++;
        }?>
  </table>
</div>
<div class="col-lg-6">
  <h2>For Each Loop</h2>
  <table class="table table-striped">   
   <?php
   		// forach loops work great with associative arrays
        foreach ($associative_array_cars as $make => $model) {
            echo "<tr><td>$make</td><td>$model</td></tr>";
        } ?>
  </table>
</div>
<div class="col-lg-6">
  <h2>Do While Loop</h2>
  <table class="table table-striped"><?php
      $i = 0;
	  do {
    	 print "<tr><td>$cars[$i]</td></tr>";
    	$i++;
	} while ($i <= 2);?>
  </table>
</div>
<div class="col-lg-6">
<table class="table table-striped">
    <h2>For Each Loop</h2>    <?php
        foreach ($cars as $car) {
            echo "<tr><td>$car</td></tr>";
        }?>
  </table>
  
</div>
<?php
}
require_once ('../inc/footer.php');?>
