<?php $title = '2.2 Arrays'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.2_arrays.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php

// numeric array
$cars1 = array('Honda', 'Nissan', 'Ford');

 print_r($cars1);  // see the contents emptied out
 print '<br />';

 if (isset($cars1[3])) { // pop out a specific index of the numeric array by its index of 3
    echo $cars1[3] . '<br />';
 } else {
     echo 'Car does not exist. <br />';
 } 

// associative array
$cars2 =  array(
    'Ford' => 'F150',
    'Nissan' => 'Pathfinder',
    'Honda' => 'Civic',
    'Toyota' => 'Camry'
); // Pop out a specific index of an associative array

if (isset($cars2['Honda'])) {
    echo $cars2['Honda'] . '<br />';
} else {
    echo 'Car does not exist <br />';
}
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>