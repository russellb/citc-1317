<?php $title = '2.5 Global Variables'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.5_global_variables.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php

$class = 'CITC-1317'; // declare a local variable 
$description = 'Intro to Scripting Languages'; // declare a local variable

function concat()
{	
	@$GLOBALS['a']  = $GLOBALS['class'] . ' ' . $GLOBALS['description'];
}
// In the example above, since a is a variable present within the $GLOBALS array, it is also accessible from outside the function!

concat(); // this calls the function

print $a; 

?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>