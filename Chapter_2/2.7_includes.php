<?php $title = '2.7 Includes'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.7_includes.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>
<div class="col-lg-12">  <?php   
	// we are putting functions in an include file and 
	// referencing them with this include
    require_once('./../inc/functions.php'); 	
	// associative array
$cars =  array(
    'Ford' => 'F150',
    'Nissan' => 'Pathfinder',
    'Honda' => 'Civic',
    'Toyota' => 'Camry'
);	
print pluck($cars, 'Ford'); // call a function from the include file to return the index "Ford"

// numeric array
$cars2 = array('Honda', 'Nissan', 'Ford');
print '<br />';
print pluck($cars2, 0) // call a function from the include file to return the first index	
?>
</div>
<?php
}
require_once ('../inc/footer.php');?>
