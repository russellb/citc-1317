<?php $title = '2.2 Conditions'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.2_conditions.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php

$first_name = 'Jon';
$last_name = 'Doe';

if ($first_name == 'jon' || $last_name == 'doe') {
    echo 'The first condition is true';
} else if ($first_name == 'Jon' && $last_name == 'Doe') {
    echo 'The second condition is true';
} else {
    echo 'The conditions are false';
}

?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>