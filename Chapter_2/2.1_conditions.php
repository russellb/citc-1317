<?php $title = '2.1 Conditions'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.1_conditions.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
// If you want to execute some code if one of the several conditions are true use the elseif statement
$first_name = 'Jon';
$last_name = 'Doe';
// Notice Case Sensitivity
if ($first_name == 'jon' || $last_name == 'doe') {
    echo 'The first condition is true';
} else if ($first_name == 'Jon' && $last_name == 'Doe') {
    echo 'The second condition is true';
} else {
    echo 'The conditions are false';
}

print '<br />';
// USE IF ELSE IF YOU WANT TO EXECUTE SOME CODE IF A CONDITION IS TRUE
// AND OTHER CODE IF THE CONDITION IS FALS
$magic_number = 8;

if($magic_number >= 5)
    print '<br />The number is greater than or equal to 5';
else 
    print '<br />The Number is less than 5';
    
if($magic_number == 6)
    print '<br /> You guessed right the number is 8!!!!';
else 
    print '<br />The Number is not 6';
    
if($magic_number < 7)
    print '<br /> The number is less than 7';
else 
    print '<br />The number is greater than 7';
    
if($magic_number < 10)
    print '<br /> The number is less than 10';
else 
    print '<br />The number is greater than 10';
    
if($magic_number == 8)
    print '<br /> You guessed right the number is 8!!!!';
else 
    print '<br />The number is not 8';

?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>