<?php $title = '2.3.1 Switch Statement'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('2.2.1_switch_statement.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{
    //If you want to select one of many blocks of code to be executed, use the Switch statement.
    //The switch statement is used to avoid long blocks of if..elseif..else code.
    $inventory_id = 'N4907a';

    switch ($inventory_id) 
    {
    case "F4150k":
        echo "Ford F150 4x4 King Cab";
        break;
    case "H4r55":
        echo "Honda Accord SV";
        break;
    case "N4907a":
        echo "Nissan Armada";
        break;
    default:
        echo "Vehicle not found";
    }

}
require_once ('../inc/footer.php');?>

