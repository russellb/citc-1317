<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>CITC-1317: Section
<?= $title; ?>
Examples</title>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="../assets/css/citc-1317-example.css" rel="stylesheet" />
 <script>
        //if clicked Yes open new page if Cancel stay on the page 
        function popup(){
        var mvar = '<?php echo $source;?>';
        var r=confirm(mvar);
        if (r==true)
        {
           // window.location = "http://example.com";
        }       
    }
    </script>
    <style>
	
	b{
		color:#F00;
		margin-left: 100px;
	}
	</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container"> <a class="navbar-brand" href="http://localhost/citc-1317/index.php">CITC-1317 Home</a> </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col-lg-12" style="text-align:center">
      <h1 class="mt-5">Section
        <?= $title ?>
        Example</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <h3>Source Code</h3>
      <!-- Source Code here -->
      <?= $source ?>
    </div>
    <div class="col-lg-6">
      <h3>Output from Source Code</h3>
      <!-- Output here -->
      <?= $output ?>
    </div>
  </div>
</div>
</body>
</html>