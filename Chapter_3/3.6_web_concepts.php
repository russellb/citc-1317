<?php $title = '3.6 Web Concepts'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('3.6_web_concepts.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>

<div class="col-lg-12">
  <?php
########################## Browser Detection ####################################
function getBrowser() { 
$u_agent = $_SERVER['HTTP_USER_AGENT']; 
$bname = 'Unknown';
$platform = 'Unknown';
$version = "";            
//First get the platform?
if (preg_match('/linux/i', $u_agent)) {
	$platform = 'linux';
}elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	$platform = 'mac';
}elseif (preg_match('/windows|win32/i', $u_agent)) {
	$platform = 'windows';
}            
// Next get the name of the useragent yes seperately and for good reason
if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
	$bname = 'Internet Explorer';
	$ub = "MSIE";
            } elseif(preg_match('/Firefox/i',$u_agent)) {
	$bname = 'Mozilla Firefox';
	$ub = "Firefox";
            } elseif(preg_match('/Chrome/i',$u_agent)) {
	$bname = 'Google Chrome';
	$ub = "Chrome";
            }elseif(preg_match('/Safari/i',$u_agent)) {
	$bname = 'Apple Safari';
	$ub = "Safari";
            }elseif(preg_match('/Opera/i',$u_agent)) {
	$bname = 'Opera';
	$ub = "Opera";
            }elseif(preg_match('/Netscape/i',$u_agent)) {
	$bname = 'Netscape';
	$ub = "Netscape";
            }            
// finally get the correct version number
$known = array('Version', $ub, 'other');
$pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';            
if (!preg_match_all($pattern, $u_agent, $matches)) {
               // we have no matching number just continue
}            
// see how many we have
$i = count($matches['browser']);            
if ($i != 1) {
//we will have two since we are not using 'other' argument yet               
//see if version is before or after the name
	if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
		$version= $matches['version'][0];
	}else {
		$version= $matches['version'][1];
	}
}else {
	$version= $matches['version'][0];
}            
// check if we have a number
if ($version == null || $version == "") {$version = "?";}
	return array(
					'userAgent' => $u_agent,
					'name'      => $bname,
					'version'   => $version,
					'platform'  => $platform,
					'pattern'   => $pattern
				);
}         
// now try it
print '<h3>Detect Browser Example</h3>';
$ua = getBrowser();
$yourbrowser = "Your browser: " . $ua['name'] . " " . $ua['version'] .
            " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];         
print_r($yourbrowser);
########################## Header Redirects ####################################
  if( @$_POST["location"] ) {
      $location = $_POST["location"];
      header( "Location:$location" );      
      exit();
   }?>
  <h3>Header Redirect Example</h3>
  <form action = "<?php $_SERVER['PHP_SELF'] ?>" method ="POST">
    <select name = "location">
      <option value = "http://www.northeaststate.edu">Northeast State</option>
      <option value = "http://www.google.com">Google</option>
    </select>
    <input type = "submit" />
  </form>
  
 <!-- ########################## Header Redirects #################################### -->
  <h3>Random Generation</h3>
  <?php
	$ran = rand( 1, 100 );
	print 'I just created my first randomness between 1 and 100 in PHP: <font color="#FF0000">' . $ran . '</font>';	  
	?>
</div>
<?php
}
require_once ('../inc/footer.php');

