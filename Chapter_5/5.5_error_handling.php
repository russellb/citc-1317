<?php $title = '5.5 Basic Error Handling '; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.5_error_handling.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
    <h3>Using the die() function</h3>
    <br />
    <?php
    // The variable is not defined and will normally print a warning to the screen
    if(isset($this_will_cause_an_error))
    {        
        print $this_will_cause_an_error;
    }
    else
    {
        // this will be a custom error sent to the browser
        // you can also log or email errors from here.
        die(print 'variable not defined!!!');
    }    
    ?> 
 </div> 
<?php
}
require_once ('../inc/footer.php');
