<?php $title = '5.4 Combine Sanitization and Validation'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.4_combine_validation_sanitize.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
    <h3>Email</h3>
<?php
$email = "jo/\hn.doe@nort//heaststate.edu";
print '<strong>This email has invalid characters:</strong> ' . $email . '<br />';

// Remove all illegal characters from email
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
print '<strong>The invalid characters were sanitized using filter_var():</strong> ' . $email . '<br />';  

// Validate email
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
    print("$email is a valid email address");
} else {
    print("$email is not a valid email address");
}

?> 
  <h3>URL</h3>
   <?php
   $url = "https://w��ww.north��eaststate.edu";
   print '<strong>This URL has invalid characters:</strong> ' . $url . '<br />';

// Remove all illegal characters from a url
$url = filter_var($url, FILTER_SANITIZE_URL);
print '<strong>The invalid characters were sanitized using filter_var():</strong> ' . $url . '<br />';  
// Validate url
if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
    echo("$url is a valid URL");
} else {
    echo("$url is not a valid URL");
}
   ?>
 </div> 
<?php
}
require_once ('../inc/footer.php');
