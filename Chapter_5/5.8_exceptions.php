<?php $title = '5.8 Exceptions '; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.8_exceptions.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
//create function with an exception
function checkNum($number) 
{
  if($number>1) 
  { // throw the exeption here
    throw new Exception("Value must be 1 or below"); 
  }
  return true;
}

//trigger exception in a "try" block
try {
  checkNum(2);
  //If the exception is thrown, this text will not be shown
  echo 'check passsed exception not thrown!!!';
}

//catch exception
catch(Exception $e) {
  echo 'Message: ' .$e->getMessage();
}
?>
 
 </div> 
<?php
}
require_once ('../inc/footer.php');
