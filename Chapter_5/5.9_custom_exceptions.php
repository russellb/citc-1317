<?php $title = '5.9 Custom Exceptions'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.9_custom_exceptions.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php

//The class must be an extension of the exception class. DONT FORGET THIS!!!
class customException extends Exception {
  public function errorMessage() {
    //error message
    $errorMsg = 'Error on line '.$this->getLine().' in '.$this->getFile()
    .': <b>'.$this->getMessage().'</b> is not a valid E-Mail address.'
    . '<br /> <strong>I just created my first custom eception class!!!!</strong>';
    return $errorMsg;
  }
}

$email = "jon.doe@northeaststate"; // this is an invalid email (no top level domain)

try {
  //check if
  if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
    //throw exception if email is not valid
    throw new customException($email);
  }
}

catch (customException $e) {
  //display custom message
  echo $e->errorMessage();
}
?>
 
 </div> 
<?php
}
require_once ('../inc/footer.php');
