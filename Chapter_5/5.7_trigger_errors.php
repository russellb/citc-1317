<?php $title = '5.7 Trigger Errors'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.7_trigger_errors.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr<br>";
  echo "I just triggered my first error!!!";
  die(); // threw in an optional die(); statement
}

//set error handler
set_error_handler("customError",E_USER_WARNING);

//trigger error
$int=2;
if ($int>=1) {
  trigger_error("Value must be 1 or below",E_USER_WARNING);
}
?>
 
 </div> 
<?php
}
require_once ('../inc/footer.php');
