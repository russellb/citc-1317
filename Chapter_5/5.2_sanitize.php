<?php $title = '5.2 Sanitize with Filters '; 

if(@$_REQUEST['view_source']==true){ $source = show_source('5.2_sanitize.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$email = "jo/\hn.doe@nort//heaststate.edu";
print '<strong>This email has invalid characters:</strong> ' . $email . '<br />';

// Remove all illegal characters from email
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
print '<strong>The invalid characters were sanitized using filter_var():</strong> ' . $email . '<br />';    
?> 
 </div> 
<?php
}
require_once ('../inc/footer.php');
