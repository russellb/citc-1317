<?php $title = '9.1 Hello World Object'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('9.1_hello_world_object.php', true); }
require_once('../inc/header.php');

/**
 * Hello World OOP Style
 * User: robowman
 * Date: 5/31/2017
 * Time: 10:39 AM
 */
class hello_world
{
    /**
     * @var
     */
    private $values = array();
   /**
    * hello_world constructor.
   */
   public  function   __construct()
   {

   }    
   
   public function __get( $key )
   {
       return $this->values[ $key ];
   }

   public function __set( $key, $value )
   {
       $this->values[ $key ] = $value;
   }  
}	
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$hw = new hello_world();
$hw->__set('test', 'Hello World!!');
print $hw->__get('test');  
?> 
 </div> 
<?php
}
require_once ('../inc/footer.php');
?>

