<?php $title = " Intro to Scripting Languages"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>CITC-1317:
<?= $title; ?>
</title>
<link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="./assets/css/citc-1317-example.css" rel="stylesheet" />
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container"> <a class="navbar-brand" href="#">CITC-1317:
    <?= $title; ?>
    Examples </a> <a class="navbar-brand" href="http://localhost/citc-1317/book/CITC-1317 Introduction to Scripting Languages.pdf">Book</a></div>
</nav>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 1 Introduction</h3>
      <ul>
        <li><a href="Chapter_1/1.1_introduction.php">1.1 Introduction - Hello World</a></li>
        <li><a href="Chapter_1/1.2_comments.php">1.2 Comments</a></li>
        <li><a href="Chapter_1/1.3_error_reporting.php">1.3 Error Reporting</a></li>
        <li><a href="Chapter_1/1.4_case_sensitivity.php">1.4 Case Sensitivity</a></li>
        <li><a href="Chapter_1/1.5_variables.php">1.5 Variables</a></li>
        <li><a href="Chapter_1/1.6_data_types.php">1.6 Data Types</a></li>
        <li><a href="Chapter_1/1.7_operators.php">1.7 Operators</a></li>
        <li><a href="Chapter_1/1.8_constants.php">1.8 Constants</a></li>
      </ul>
    </div>
    <div class="col-lg-6">
      <h3>Chapter 2 The PHP Language</h3>
      <ul>
        <li><a href="Chapter_2/2.1_conditions.php">2.1 Conditions and Decisions</a></li>
        <li><a href="Chapter_2/2.2.1_switch_statement.php">2.2.1 Case Statement</a></li>
        <li><a href="Chapter_2/2.2_arrays.php">2.2 Arrays</a></li>
        <li><a href="Chapter_2/2.3_loops.php">2.3 Loops</a></li>
        <li><a href="Chapter_2/2.4_functions.php">2.4 Functions</a></li>
        <li><a href="Chapter_2/2.5_global_variables.php">2.5 Global Variables</a></li>
        <li><a href="Chapter_2/2.6_super_globals.php">2.6 Superglobals</a></li>
        <li><a href="Chapter_2/2.7_includes.php">2.7 Includes</a></li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 3 User Input, Stateful Web Applications and other Concepts</h3>
      <ul>
        <li><a href="Chapter_3/3.1_get.php">3.1 GET</a></li>
        <li><a href="Chapter_3/3.2_post.php">3.2 POST</a></li>
        <li><a href="Chapter_3/3.3_request.php">3.3 REQUEST</a>
        <li><a href="Chapter_3/3.4_sessions.php">3.4 Sessions</a></li>
        <li><a href="Chapter_3/3.5_cookies.php">3.5 Cookies</a></li>
        <li><a href="Chapter_3/3.6_web_concepts.php">3.6 Web Concepts</a></li>
      </ul>
    </div>
    <div class="col-lg-6">
      <h3>Chapter 4 Forms</h3>
      <ul>
        <li><a href="Chapter_4/4.1_simple_form.php">4.1 A Simple Form</a></li>
        <li><a href="Chapter_4/4.2_form_validation.php">4.2 Form Validation</a></li>
        <li><a href="Chapter_4/4.3_form_required_fields.php">4.3 Form Required Fields</a></li>
        <li><a href="Chapter_4/4.4_advanced_form_validation.php">4.4 Advanced Form Validation</a></li>
        <li><a href="Chapter_4/4.5_finished_form.php">4.5 Finished Form</a></li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 5 Validate and Sanitize Data -- Error Handling and Exceptions</h3>
      <ul>
        <li><a href="Chapter_5/5.1_filter_list.php">5.1 Filters</a></li>
        <li><a href="Chapter_5/5.2_sanitize.php">5.2 Sanitize with Filters</a></li>
        <li><a href="Chapter_5/5.3_validate.php">5.3 Validate with Filters</a></li>
        <li><a href="Chapter_5/5.4_combine_validation_sanitize.php">5.4 Combine Validation and Sanitization</a></li>
        <li><a href="Chapter_5/5.5_error_handling.php">5.5 Basic Error Handling</a></li>
        <li><a href="Chapter_5/5.6_custom_error_handling.php">5.6 Custom Error Handling</a></li>
        <li><a href="Chapter_5/5.7_trigger_errors.php">5.7 Trigger Errors</a></li>
        <li><a href="Chapter_5/5.8_exceptions.php">5.8 Exceptions</a></li>
        <li><a href="Chapter_5/5.9_custom_exceptions.php">5.9 Custom Exceptions</a></li>
      </ul>
    </div>
     <div class="col-lg-6">
      <h3>Chapter 6 MySQL</h3>
      <ul>
        <li><a href="Chapter_6/6.1_mysql_connection.php">6.1 MySQL Connection</a></li>
        <li><a href="Chapter_6/6.2_mysql_create_database.php">6.2 MySQL Create Database</a></li>
        <li><a href="Chapter_6/6.3_mysql_create_table.php">6.3 MySQL Create Table</a></li>
        <li><a href="Chapter_6/6.4_mysql_insert.php">6.4 MySQL Insert Data</a></li>
        <li><a href="Chapter_6/6.5_mysql_select.php">6.5 MySQL Select Data</a></li>
        <li><a href="Chapter_6/6.6_mysql_update.php">6.6 MySQL Update Data</a></li>
        <li><a href="Chapter_6/6.7_mysql_delete.php">6.7 MySQL Delete Data</a></li>
        <li><a href="Chapter_6/6.8_mysql_truncate.php">6.8 MySQL Truncate Data (Reset Students Table)</a></li>
      </ul>
    </div>   
  </div>
  <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 7 Advanced PHP</h3>
      <ul>
        <li><a href="Chapter_7/7.1_multi_dimensional_arrays.php">7.1 Multi Dimensional Arrays</a></li>
        <li><a href="Chapter_7/7.2_array_sorting.php">7.2 Sorting Arrays</a></li>
        <li><a href="Chapter_7/7.3_date_time_functions.php">7.3 Date and Time Functions</a></li>
        <li><a href="Chapter_7/7.4_regular_expressions.php">7.4 Regular Expressions</a></li> 
        <li><a href="Chapter_7/7.5_file_handling.php">7.5 File Handling</a></li>
        <li><a href="Chapter_7/7.6_xml_dom.php">7.6 XML DOM</a></li>
        <li><a href="Chapter_7/7.7_using_generators.php">7.7 Using Generators</a></li>
      </ul>
    </div>
    <div class="col-lg-6">
      <h3>Chapter 8 AJAX</h3>
      <ul>
        <li>8.1 AJAX Hello World</li>
        <li>8.2 AJAX and PHP (text hints)</li>
        <li>8.3 AJAX Database</li>
        <li>8.4 AJAX Search</li>
        <li>8.5 AJAX and Files</li>
        <li>8.6 AJAX and XML</li>
      </ul>
    </div>
  </div>
    <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 9 Objects</h3>
      <ul>
          <li><a href="Chapter_9/9.1_hello_world_object.php">9.1 Hello World Object</a></li>
      </ul>
    </div>
    <div class="col-lg-6">
      <h3>Chapter 10 Dependency Management</h3>
      <ul>
        <li></li>
      </ul>
    </div>
  </div>
    <div class="row">
    <div class="col-lg-6">
      <h3>Chapter 11 Frameworks</h3>
      <ul>
        <li></li>
      </ul>
    </div>
    <div class="col-lg-6">
      <h3>Chapter 12 Restful API</h3>
      <ul>
        <li></li>
      </ul>
    </div>
  </div>
</div>
</div>
</body>
</html>
