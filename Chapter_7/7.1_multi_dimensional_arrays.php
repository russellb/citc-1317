<?php $title = '7.1 Multi Dimensional Arrays'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('7.1_multi_dimensional_arrays.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-6"> 
    <h3>Example 1 Array</h3>
<?php
// CREATE A 2 DIMENSIONAL ARRAY
$cars = array( 
                array( 'Nissan', 'Altima', 22000 ),
                array( 'Nissan', 'Pathfinder', 45000 ),
                array( 'Nissan', 'Sentra', 17000 ) 
            );
// DUMP CONTENTS ON THE SCREEN
print '<pre>';
print_r($cars);
print '</pre>';
print '</div>';
print '<div class="col-lg-6">';
print '<h3>Example 1 Output</h3>';
print '<br />';
// USE FOREACH TO PARSE ARRAY
foreach($cars as $car){
    print '<strong>Make:</strong> ' . $car[0] . ' <strong>Model:</strong> ' . $car[1] . ' <strong>Price:</strong> ' . $car[2] . '<br />';
}

?>
 </div> 
<div class="col-lg-6">
    <h3>Example 2 Array</h3>
<?php
// CREATE NEW 2D ARRAY WITH ASSOCIATE ARRAY
$cars = array( 
                array( 'Make' => 'Nissan', 'Model' => 'Altima', 'Price' => 22000 ),
                array( 'Make' => 'Nissan', 'Model' => 'Pathfinder', 'Price' =>  45000 ),
                array( 'Make' => 'Nissan', 'Model' => 'Sentra', 'Price' =>  17000 ) 
            );
// DUMP CONTENTS ON THE SCREEN
print '<pre>';
print_r($cars);
print '</pre>';
print '</div>';
print '<div class="col-lg-6">';
print '<h3>Example 2 Output</h3>';
// USE FOREACH TO PARSE ARRAY NOW WE CAN EASILY POP OUT BY INDEX
foreach($cars as $car){
    if($car['Model'] == 'Pathfinder')
            print '<strong>Make:</strong> ' . $car['Make'] . ' <strong>Model:</strong> ' . $car['Model'] . ' <strong>Price:</strong> ' . $car['Price'] . '<br />';
    else
        continue;
   
}
?>    
</div>
    <?php

}
require_once ('../inc/footer.php');