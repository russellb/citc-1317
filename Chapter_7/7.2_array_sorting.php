<?php $title = '7.2 Sorting Arrays'; 
if(@$_REQUEST['view_source']==true){ $source = show_source('7.2_array_sorting.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-6"> 
<h3>Traditional Arrays</h3>
<?php
print '<strong>Original Array</strong><br />';
$cars = array('Nissan', 'Honda', 'Ford', 'Toyota', 'Chevrolet');        
print '<pre>';
var_dump($cars);
print '<strong>Ascending with sort()</strong><br />';
sort($cars);   
var_dump($cars);
print '<strong>Descending with rsort()</strong><br />';
rsort($cars);
var_dump($cars);
print '</pre>';
?>
</div>
<div class="col-lg-6"> 
<h3>Associative Arrays</h3>
<?php
print '<strong>Descending with rsort()</strong><br />';
$cars_assoc = array('Nissan' => 'Altima', 'Ford' => 'F150', 'Honda' => 'Accord', 'Toyota' => 'Tundra', 'Chevrolet' => 'Camaro');
print '<strong>Original Associative Array</strong><br />';
print '<pre>';
var_dump($cars_assoc);
print '<strong>Ascending According to Value with asort()</strong><br />';
asort($cars_assoc);
var_dump($cars_assoc);
print '<strong>Ascending According to Key with ksort()</strong><br />';
ksort($cars_assoc);
var_dump($cars_assoc);
print '<strong>Descending According Value with arsort()</strong><br />';
arsort($cars_assoc);
var_dump($cars_assoc);
print '<strong>Descending According Key with krsort()</strong><br />';
krsort($cars_assoc);
var_dump($cars_assoc);
print '</pre>';  
?>
</div> 
<?php
}
require_once ('../inc/footer.php');