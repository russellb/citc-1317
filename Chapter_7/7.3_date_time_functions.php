<?php $title = '7.3 Date Time Functions'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('7.3_date_time_functions.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-6"> 
    <h3>Today Examples</h3> 
    <?php
    print "Today is " . date("Y-m-d") . "<br />";
    print "The current time is " . date("h:i:sa") .'<br />';
    print "Today is " . date("l") . '<br />';
    date_default_timezone_set("America/New_York");
    print "The time in the Eastern US Time Zone is is " . date("h:i:sa") . '<br /><br />';    
    ?>
    <h3>Get date of next 4 Fridays</h3>
    <?php
    //Get the Month and Day of the next 4 Fridays
    $startdate = strtotime("Friday");
    $enddate = strtotime("+4 weeks", $startdate);

    while ($startdate < $enddate) {
        print date("M d", $startdate) . "<br>";
        $startdate = strtotime("+1 week", $startdate);
    }
    print '<br />';
?>
 </div> 
<div class="col-lg-6"> 
    <h3>Make Time Example</h3>
    <?php
    
    $date=mktime(20, 18, 00, 7, 20, 1969);
    print "I just created the date " . date("Y-m-d h:i:sa", $date) . '<br />';  
     $date=mktime(02, 56, 15, 7, 21, 1969);
    print "I just created another date " . date("Y-m-d h:i:sa", $date) . '<br /><br>';  
    ?>
    <h3>Date Countdown</h3>
    <?php
    $d1=strtotime("December 10");
    $d2=ceil(($d1-time())/60/60/24);
    print "There are " . $d2 ." days until finals start. <br />";
    ?>
    <h3>More strings to Time -- strtotime()</h3>
    <?php
    $date=strtotime("11:59:59pm December 31 2018");
    print "Created this date with strings " . date("Y-m-d h:i:sa", $date) . '<br />';
    $date=strtotime("tomorrow");
    echo date("Y-m-d h:i:sa", $date) . "<br />";

    $date=strtotime("next Friday");
    echo date("Y-m-d h:i:sa", $date) . "<br />";

    $date=strtotime("+6 Months");
    echo date("Y-m-d h:i:sa", $date) . "<br />";;
    
    
    ?>
</div>
<?php
}
require_once ('../inc/footer.php');