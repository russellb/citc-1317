<?php $title = '7.7 Using Generators'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('7.7_using_generators.php', true); }
require_once('../inc/header.php');
$course = '';
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-6"> 
<?php

$gen = (function() { 
					yield "First Yield"; 
					yield "Second Yield"; 
					return "return Value" ;})(); 
	
foreach ($gen as $val) 
{ 
	echo $val, PHP_EOL;
} 
	
echo $gen->getReturn() , PHP_EOL;


 // It will give the output as: First Yield Second Yield return Value So it clearly shows that calling a generator function in foreach will not return the return statement. Instead, it will just return at every yield. To get the return Value , this syntax: $gen->getReturn() can be used.
?>
 </div> 
<?php
}
require_once ('../inc/footer.php');