<?php  
$title = '6.6 MySQL Update'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.6_mysql_update.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "UPDATE students SET last_name='Smith', class='CITC-1317' WHERE id=2";

    // Prepare statement
    $stmt = $conn->prepare($sql);

    // execute the query
    $stmt->execute();

    // echo a message to say the UPDATE succeeded
    print $stmt->rowCount() . " records UPDATED successfully";
    
    // select all users
    $stmt = $conn->query("SELECT id, first_name, last_name, email, class FROM students WHERE id=2");
    
    print '<table>';
    while ($row = $stmt->fetch()) 
    {
        print '<tr>';
        print '<td>' . $row['id'] . '</td>';
        print '<td>' . $row['first_name'] . '</td>';
        print '<td>' . $row['last_name'] . '</td>'; 
		print '<td>' . $row['email'] . '</td>'; 
		print '<td>' . $row['class'] . '</td>'; 
        print '</tr>';
    }
    print '</table>';
    
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }

$conn = null;
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>



