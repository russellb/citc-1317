<?php $title = '6.1 MySQL Connection'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.1_mysql_connection.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    print "Connected successfully"; 
    }
catch(PDOException $e)
    {
    print "Connection failed: " . $e->getMessage();
    } 
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>