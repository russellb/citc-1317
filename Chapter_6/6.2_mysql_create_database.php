<?php 
$title = '6.2 MySQL Create Databse'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.2_mysql_create_database.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Create database
$sql = "CREATE DATABASE chapter7";
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully<br />";
} else {
    print "Error creating database: " . $conn->error;
}

// drop the  database to prevent errors for database already exists in the above script
$sql = "DROP DATABASE chapter7";
if ($conn->query($sql) === TRUE) {
    print "Database dropped successfully";
} else {
    print "Error dropping database: " . $conn->error;
}

$conn->close();
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>

