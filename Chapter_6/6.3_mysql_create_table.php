<?php 
$title = '6.3 MySQL Create Table'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.3_mysql_create_table.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // sql to create table
    $sql = "CREATE TABLE Cars (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    make VARCHAR(30) NOT NULL,
    model VARCHAR(30) NOT NULL,
    year VARCHAR(50),
    recieved_on TIMESTAMP
    )";

    // use exec() because no results are returned
    $conn->exec($sql);
    print "Table Cars created successfully<br />";
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }
    
    try {    

    // sql to drop table
    $sql = "DROP TABLE Cars";

    // use exec() because no results are returned
    $conn->exec($sql);
    print "Table Cars dropped successfully";
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }

$conn = null;
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>



