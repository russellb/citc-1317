<?php 
$title = '6.8 MySQL Truncate Example Table'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.8_mysql_truncate.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Remove all example data so you can start again
    $sql = "TRUNCATE students";

    // use exec() because no results are returned
    $conn->exec($sql);
    print "Table Truncated successfully";
    }
catch(PDOException $e)
    {
    print $sql . "<br />" . $e->getMessage();
    }

$conn = null;
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>



