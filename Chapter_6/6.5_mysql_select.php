<?php
$title = '6.5 MySQL Select'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.5_mysql_select.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-6"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT id, first_name, last_name, email FROM students";
    
    // select all users
    $stmt = $conn->query($sql);
    
    print '<h4>SELECT id, first_name, last_name, email FROM students</h4>';
    print '<table>';
    while ($row = $stmt->fetch()) 
    {
        print '<tr>';
        print '<td>' . $row['id'] . '</td>';
        print '<td>' . $row['first_name'] . '</td>';
        print '<td>' . $row['last_name'] . '</td>'; 
		print '<td>' . $row['email'] . '</td>';
        print '</tr>';
    }
    print '</table>';
    
    }
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }
print '</div>';
print '<div class="col-lg-6">';

//  LIMIT 5 results

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT * FROM students LIMIT 5";   
    
    // select a LIMIT of 5
    $stmt = $conn->query($sql);
    print '<h4>SELECT * FROM students LIMIT 5</h4>';
    print '<table>';
    while ($row = $stmt->fetch()) 
    {
        print '<tr>';
        print '<td>' . $row['id'] . '</td>';
        print '<td>' . $row['first_name'] . '</td>';
        print '<td>' . $row['last_name'] . '</td>'; 
		print '<td>' . $row['email'] . '</td>'; 
        print '</tr>';
    }
    print '</table>';
    
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }
// LIMIT 5 starting at 5th record    
 try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT * FROM students LIMIT 2, 2";
   
    // select a LIMIT of 5 strting at the 5th record
    $stmt = $conn->query($sql);
    print '<h4>SELECT * FROM students LIMIT 2, 2</h4>';
    print '<table>';
    while ($row = $stmt->fetch()) 
    {
        print '<tr>';
        print '<td>' . $row['id'] . '</td>';
        print '<td>' . $row['first_name'] . '</td>';
        print '<td>' . $row['last_name'] . '</td>';		
        print '<td>' . $row['email'] . '</td>';  
        print '</tr>';
    }
    print '</table>';
    
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }  
$conn = null;
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>

