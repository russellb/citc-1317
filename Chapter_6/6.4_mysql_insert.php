<?php
$title = '6.4 MySQL Insert'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('6.4_mysql_insert.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO students (first_name, last_name, email)
    VALUES ('John', 'Doe', 'john@nescc.edu')";
    // use exec() because no results are returned
    $conn->exec($sql);
    $last_id = $conn->lastInsertId();
    print "New record created successfully. <br /> Last inserted ID is: " . $last_id . '<br />';
    }
catch(PDOException $e)
    {
    print $sql . "<br>" . $e->getMessage();
    }
    
    try {    // begin a transaction to do multiple inserts
    $conn->beginTransaction();
    // build SQL statements
    $conn->exec("INSERT INTO students (first_name, last_name, email) 
    VALUES ('James', 'Doe', 'james@nescc.edu')");
    $conn->exec("INSERT INTO students (first_name, last_name, email) 
    VALUES ('Nancy', 'Doe', 'mary@nescc.edu')");
    $conn->exec("INSERT INTO students (first_name, last_name, email) 
    VALUES ('Jackie', 'Doe', 'jackie@nescc.edu')");

    // commit the transaction
    $conn->commit();
    print "New records created successfully by a transaction<br />";
    }
catch(PDOException $e)
    {
    // roll back the transaction if something failed
    $conn->rollback();
    print "Error: " . $e->getMessage();
    }
    
// Prepared Statements
try {
   

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO students (first_name, last_name, email) 
    VALUES (:firstname, :lastname, :email)");
    $stmt->bindParam(':firstname', $firstname);
    $stmt->bindParam(':lastname', $lastname);
    $stmt->bindParam(':email', $email);

    // insert a row
    $firstname = "Jeff";
    $lastname = "Doe";
    $email = "jeff@nescc.edu";
    $stmt->execute();

    // insert another row
    $firstname = "Jill";
    $lastname = "Doe";
    $email = "jill@nescc.edu";
    $stmt->execute();

    // insert another row
    $firstname = "Joe";
    $lastname = "Doe";
    $email = "joe@nescc.edu";
    $stmt->execute();

    print "New records created successfully by prepared statements";
    }
catch(PDOException $e)
    {
    print "Error: " . $e->getMessage();
    }
$conn = null;
?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>



