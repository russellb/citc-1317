<?php $title = '4.5 Finished Form'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('4.5_finished_form.php', true); }
require_once('../inc/header.php');
// new functionality to clean and make the form more secure
function prep_data($data) {
  $data = trim($data);
  $data = stripslashes($data); // remove slashes if data coming from a database
  $data = htmlspecialchars($data); // help prevent cross site scripting attacks
  return $data;
}

// define variables and set to empty values
$name = $email = $url = $formErr =  ""; // New var to contain errors
$submit = false; // detect if form submission occured.
// The following boolean values are set to let the form know if the server side validation was a success or failure.
// If it failed then a bootstrap 4 alert will be triggered with the field borders changing to the color red
$invalidName = false; // used in new form validation
$invalidEmail = false; // used in new form validation
$invalidURL = false; // used in new form validation

// validate required fields and build error string to display on viewport if necessary
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if (empty($_POST["name"]))
	{
    	$formErr .= "Name is required. <br />"; // append results in case of mulitiple errors
  	} 
	else 
	{
   	 	$name = prep_data($_POST["name"]);
		if (!preg_match("/^[a-zA-Z ]*$/",$name)) 
		{  		
			$formErr .= '<div class="alert alert-danger fade show" role="alert">
  							<strong>Form Submission Error:</strong> Only letters and spacse allowed in the name field.
						</div>';
			$invalidName = true;
		}
  	}	
	if (empty($_POST["email"]))
	{
    	$formErr .= "Email is required. <br />"; // append results in case of mulitiple errors
  	} 
	else 
	{
   	 	$email = prep_data($_POST["email"]);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
  			
			$formErr .= '<div class="alert alert-danger fade show" role="alert">
  							<strong>Form Submission Error:</strong> Invalid email format, Use the mymail@mysite.com format.
						</div>';
			$invalidEmail = true;
		}
  	}	
	if (empty($_POST["url"]))
	{
    	$url = "";
  	} 
	else 
	{
   	 	$url = prep_data($_POST["url"]);
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) 
		{			
			$formErr .= '<div class="alert alert-danger fade show" role="alert">
  							<strong>Form Submission Error:</strong> Invalid URL, Use the www.mysite.com format.
						</div>';
			$invalidURL = true;
		}
  	}   
	$submit = true;
}
if(!@$_REQUEST['view_source'])
{?>

<div class="col-lg-12">
    
    <!-- NOTICE the vule is set with the variable containing the form submission in case of error so user can see the submission -->
  <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">  
   <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="name">Name <font color="red">*</font></label>
      <!-- see the attribute required at the end of the name input field. This instructs the browser to halt the submission unless the field is populated -->
      <input type="text" class="form-control <?php if($invalidName === true){print 'is-invalid';}else{print 'is-valid'; }?>" name="name" id="name" placeholder="name" value="<?= $name ?>" required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="email">Email <font color="red">*</font></label>
      <!-- when the type is email the browser will valided email format on the client side. This also has the required attribute just like the name field above -->
      <input type="email" class="form-control <?php if($invalidEmail === true){print 'is-invalid';}else{print 'is-valid'; }?>" name="email" id="email" placeholder="Email" value="<?= $email ?>" required>
    </div>
    
  </div>
  <div class="form-row">
  <div class="col-md-8 mb-6">
      <label for="url">URL</label>
        <input type="text" class="form-control <?php if($invalidURL === true){print 'is-invalid';}else{print 'is-valid'; }?>" name="url" id="url" placeholder="http://www.mysite.com" value="<?= $url ?>" >
    </div>
  </div><br />

   
   <button class="btn" type="submit">Submit form</button>
    <button class="btn" type="reset">Reset form</button> <!-- add reset button -->
  </form><br />

  <font color="red">*</font><strong> Required Fields</strong><br /><br />

  </div>
  <div class="col-lg-12">  <?php
	if (!empty($formErr))
	{  
		print $formErr;
	}
	elseif($submit === true)
	{
		print '<div class="alert alert-success fade show" role="alert">
				Hi ' . $name . ', your chose the Website ' . $url . ', your email is ' . $email . '.
			   </div>'; 
	}?>
</div>
<?php
}
require_once ('../inc/footer.php');
?>