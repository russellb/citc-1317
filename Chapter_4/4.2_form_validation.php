<?php $title = '4.2 Form Validation - Prep Data'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('4.2_form_validation.php', true); }
require_once('../inc/header.php');
// we will use this method to prepare the data for presentation
function prep_data($data) {
  $data = trim($data);
  return $data;
}
// define variables and set to empty values
$name = $email = $url = "";
// send request to prep_data() function
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = prep_data($_POST["name"]);
  $email = prep_data($_POST["email"]);
  $url = prep_data($_POST["url"]);
}
if(!@$_REQUEST['view_source'])
{?>

<div class="col-lg-12">
    
  <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post"> <!-- Switch method to post to hide parameters -->
    Name:
    <input type="text" name="name"> 
    <br />
    eMail:
    <input type="text" name="email"> 
     <br />
    URL:
    <input type="text" name="url"> 
     <br />
    <input type="submit">
  </form>
  </div>
  <div class="col-lg-12">  <?php
 if (!empty($name) || !empty($email) || !empty($url)) {  // make sure at least one of the fields was populated.
 	print 'Hi ' . $name . ', your chose the Website ' . $url . ', your email is ' . $email . '.'; 
 }
  ?>  
</div>
<?php
}
require_once ('../inc/footer.php');
?>
