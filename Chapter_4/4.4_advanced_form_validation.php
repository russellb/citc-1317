<?php $title = '4.4 Form Validation - Advanced'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('4.4_advanced_form_validation.php', true); }
require_once('../inc/header.php');

function prep_data($data) {
  $data = trim($data);
  return $data;
}

// define variables and set to empty values
$name = $email = $url = $formErr =  ""; // New var to contain errors
$submit = false; // detect if form submission occured.

// validate required fields and build error string to display on viewport if necessary
if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
	if (empty($_POST["name"]))
	{
    	$formErr .= "Name is required. <br />"; // append results in case of mulitiple errors
  	} 
	else 
	{
   	 	$name = prep_data($_POST["name"]);
		if (!preg_match("/^[a-zA-Z ]*$/",$name)) 
  			$formErr = "Only letters and  space allowed in the name field.<br />"; 
  	}	
	if (empty($_POST["email"]))
	{
    	$formErr .= "Email is required. <br />"; // append results in case of mulitiple errors
  	} 
	else 
	{
   	 	$email = prep_data($_POST["email"]);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
  			$formErr .= "Invalid email format. <br />"; 
  	}	
	if (empty($_POST["url"]))
	{
    	$url = "";
  	} 
	else 
	{
   	 	$url = prep_data($_POST["url"]);
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) 
  			$formErr .= "Invalid URL. <br />"; 
  	}   
	$submit = true;
}
if(!@$_REQUEST['view_source'])
{?>

<div class="col-lg-12">
    
    <!-- NOTICE the vule is set with the variable containing the form submission in case of error so user can see the submission -->
  <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">  
    Name:
    <input type="text" name="name" value="<?= $name ?>" > <font color="red">*</font> must contain only letters and spaces.
    <br />
    eMail:
    <input type="text" name="email" value="<?= $email ?>"> <font color="red">*</font> must be a valid email address. <br /><!-- bootstrap has builtin clientside validation if you change the type to "email" -->
    URL:
    <input type="text" name="url" value="<?= $url ?>"> If present must be a valid URL <br />
    <input type="submit" />
    <input type="reset" /> <!-- add reset button -->
  </form>
  <font color="red">*</font> Required Fields<br />
  </div>
  <div class="col-lg-12">  <?php
	if (!empty($formErr))
	{  
		print '<br /><font color="red">' . $formErr . '</font>';
	}
	elseif($submit === true)
		print '<br />Hi ' . $name . ', your chose the Website ' . $url . ', your email is ' . $email . '.'; 
	?>
</div>
<?php
}
require_once ('../inc/footer.php');
?>