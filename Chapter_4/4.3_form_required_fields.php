<?php $title = '4.3 Form Validation - Required Fields'; 
if(@$_REQUEST['view_source']==true){ $source = show_source('4.3_form_required_fields.php', true); }
require_once('../inc/header.php');
// we will use this method to prepare the data for presentation
function prep_data($data) {
  $data = trim($data);
  return $data;
}
// define variables and set to empty values
$name = $email = $url = "";
$submit = false; // detect if form submission occured.
// send request to prep_data() function
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = prep_data($_POST["name"]);
  $email = prep_data($_POST["email"]);
  $url = prep_data($_POST["url"]);
  $submit = true;
}
if(!@$_REQUEST['view_source'])
{?>

<div class="col-lg-12">
    <font color="red">*</font> Required<br />
  <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post"> 
    Name:
    <input type="text" name="name"> <font color="red">*</font> 
    <br />
    eMail:
    <input type="text" name="email"> <font color="red">*</font>  <br /><!-- bootstrap has builtin clientside validation if you change the type to "email" -->
    URL:
    <input type="text" name="url">  <br />
    <input type="submit">
  </form>
  </div>
  <div class="col-lg-12">  <?php 
 if (!empty($name) && !empty($email)) {  // Change the operator to and to require both name and email
 	print 'Hi ' . $name . ', your chose the Website ' . $url . ', your email is ' . $email . '.'; 
 }elseif($submit === true)
 { // print the message about required fields to the viewport if condition fails
	 print "You must enter a name and email address";
 }
  ?>
</div>
<?php
}
require_once ('../inc/footer.php');
?>