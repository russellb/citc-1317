<?php

  function pluck($arr, $key) {
    if (isset($arr[$key])) { // pop out a specific index of the traditional array
       $result = $arr[$key] . '<br />';
  } else {
     $result =  'Car does not exist. <br />';
  } 
    return $result;
  }

  function sum($a, $b) {
      $result = $a + $b;

      return $result;
  }
  

  function output($value) {
      echo '<pre>';
      print_r($value);
      echo '</pre>';
  }

  function authenticate_user($email, $password) {
      return $email == USER_NAME && $password == PASSWORD;
  }

  function redirect($url) {
    header("Location: $url");
  }

  function is_user_authenticated() {
      return isset($_SESSION['email']);
  }
  
  function ensure_user_is_authenticated() {
      if (!is_user_authenticated()) {
        redirect('login.php');
        die();
      }
  }