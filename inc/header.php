<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>CITC-1317: Section
<?= $title; ?>
 Code Examples</title>
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="../assets/css/citc-1317-example.css" rel="stylesheet" />
<style>
b {
	color: #F00;
	margin-left: 100px;
}
</style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container"> 
  	<a class="navbar-brand" href="http://localhost/citc-1317/index.php">CITC-1317 Home</a> 
    <?php if(@$_REQUEST['view_source']==true) { ?>
    	<a class="navbar-brand" href="<?=  $_SERVER['PHP_SELF'] ?>">View Example </a> 
    <?php }else{ ?>
    	<a class="navbar-brand" href="<?=  $_SERVER['PHP_SELF'] ?>?view_source=true">View Source </a> 
    <?php } ?>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col-lg-12" style="text-align:center">
      <h1 class="mt-5">Section
        <?= $title ?>
        <?php if(@$_REQUEST['view_source'] == true){print 'Source';}else{print 'Example';}	?>
     </h1>
    </div>
  </div>
    <div class="row">
    <?php if(@$_REQUEST['view_source']==true){ ?>
    <div class="col-lg-12">
      <?= @$source ?>
    </div>
    <?php } ?>
   