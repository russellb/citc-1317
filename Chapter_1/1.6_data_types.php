<?php $title = '1.6 Data Types'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('1.6_data_types.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
##### TYPE JUGGLING EXAMPLES ###################
$var = "10";   //$var is a string
var_dump($var);
print '<br />';
$var  = 10;  //$var is  an integer
var_dump($var);
print '<br />';
$var  = $var * 1.5;  //$var is a float
var_dump($var);
print '<br />';
@$var  = 8 * "two plus 4";    //$var is an integer
var_dump($var);
print '<br />';
############ STRING EXAMPLES ###################
$x = "I am a String!";  // Using single quotes
$y = 'I am also a string.'; // Using double quotes
var_dump($x);
print '<br />'; 
var_dump($y);
print '<br />';
############ STRING MANIPULATION ################
// substr(); 
print substr("abcdef", -1) . '<br />';    // returns f
print substr("abcdef", -2) . '<br />';    // returns ef
print substr("abcdef", -3, 1 ) . '<br />'; // returns d
 // strlen(); returns the length of a given string
$str = "Hello world!" ;
print strlen($str ) . '<br />'; // outputs 12 --- spaces are counted
 // trim(); removes white space before and after the string
$str = "    Hello   world!      " ;
print  trim($str) . '<br />'; // outputs “Hello    world!”   notice the middle is still 3 spaces wide
// ltrim(); removes all space on the left
$str = "    Hello   world!      " ;
print  ltrim($str) . '<br />'; // will output “Hello   world!      “;
// rtrim(); removes all space on the right
$str = "    Hello   world!      " ;
print rtrim($str) . '<br />'; // will output  “    Hello   world!”
 // strtolower(); makes the entire string lower case
$str = "Hello  world! " ;
print strtolower($str . '<br />'); // will output “hello world!”
// strtoupper(); makes the entire string upper case
$str = "Hello  world! " ;
print strtoupper($str) . '<br />';  // will output “HELLO WORLD!”
// str_replace(); will replace a one string with another specified string
$str = "Hello world!";
print str_replace('Hello', 'Goodbye', $str) . '<br />';
// explode(); breaks a string into an array according to a delimiter passed in
$str = "I,Love,citc-1317";
var_dump(explode(",", $str)) . '<br />'; // will output and array
############ INTEGER EXAMPLES ###################
$x = 407;
var_dump($x) . '<br />';
print '<br />';
############ FLOAT EXAMPLES ####################
$x = 4.071;
var_dump($x) . '<br />'; // will output float(4.071) 
print '<br />';
############ BOOLEAN EXAMPLES ###################
$x = false;
var_dump($x) . '<br />'; // will output bool(false)
print '<br />';
############ NULL EXAMPLES ###################
$x = "Hello world!";
$x = NULL;
var_dump($x); // will output NULL


?>

</div> 
<?php
}
require_once ('../inc/footer.php');
?>



