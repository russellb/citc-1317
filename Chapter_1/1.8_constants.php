<?php $title = '1.8 Constants'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('1.8_constants.php', true); }
require_once('../inc/header.php');

if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
// use define() to declare a constant
// Constants are all caps
// Constants are GLOBAL variables but need no $ in the declaration
define("MYCONSTANT", "Hello World! My First Constant Variable");

function call_constant() {
    echo MYCONSTANT;
}
 
// no need to pass any arguements since constants are global
call_constant();

?>
</div> 
<?php
}
require_once ('../inc/footer.php');
?>



