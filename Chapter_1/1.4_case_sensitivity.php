<?php $title = '1.4 Case Sensitivity'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('1.4_case_sensitivity.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
     <div class="col-lg-12"> 
      <p> In PHP, all keywords (e.g. if, else, while, echo, etc.), classes, functions, and user-defined functions are NOT case-sensitive.
        
        In the example below, all three echo statements below will output results: </p>
	<?php
		ECHO "Hello World!<br>";
		echo "Hello World!<br>";
		EcHo "Hello World!<br>";

		function foobar()
		{
			return 'Hello World!';
		} 
		// All three of the below examples will work.
		foobar();
		FooBar();
		FOOBAR();
		// Variables on the other hand are case sensitive!!!
		?>
      <br />
      <p> In the example below, only the last statement will display the value of the $color variable (this is because $color, $COLOR, and $coLOR are treated as three different variables): </p>
      <?php
		$myFirstVar = "Hello World!";
		// The Next two calls will fail to output the variable
		print $myFirstVAR . '<br />';
		print $myFiRStVar . '<br />'; 
		// Only this code will work.
		print $myFirstVar;  
		?>
    </div>
    <?php } // end if 
     require_once('../inc/footer.php'); ?>