<?php $title = '1.5 Variables'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('1.5_variables.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>      
<div class="col-lg-12"> 
<?php
$txt = "CITC-1317";
echo "I love $txt!";
// Single quotes work in the declaration. 
// You must use double quote to combine echoed text with an embedded variable.
// You can also embed html inside of text (see below <br /> tag)
$txt = 'CITC-1317';
echo "<br />I love $txt!<br />";
// single quotes can be used if you concatenate the text with a variable.
$txt = 'CITC-1317';
echo 'I love ' . $txt . '!<br />';
// Run this code to see what happens when you wrap single quotes in the echo.
$txt = 'CITC-1317';
echo 'I love $txt!<br />';
?>
 </div> 
<?php
}
require_once ('../inc/footer.php');
?>
