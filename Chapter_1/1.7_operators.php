<?php $title = '1.7 Operators'; 

if(@$_REQUEST['view_source']==true){ $source = show_source('1.7_operators.php', true); }
require_once('../inc/header.php');
if(!@$_REQUEST['view_source'])
{?>
<div class="col-lg-6">
<?php
####################### ARITHMETIC OPERATORS ##########################
	print '<h3>Arithmetic Operators Examples</h3>';
	$a = 12;
	$b = 25;	
         
	$c = $a + $b;
	echo "Addtion Operation Result: $c <br/>";
         
	$c = $a - $b;
	echo "Substraction Operation Result: $c <br/>";
         
	$c = $a * $b;
	echo "Multiplication Operation Result: $c <br/>";
         
	$c = $a / $b;
	echo "Division Operation Result: $c <br/>";
         
	$c = $a % $b;
	echo "Modulus Operation Result: $c <br/>";
         
	$c = $a++; 
	echo "Increment Operation Result: $c <br/>";
         
	$c = $a--; 
	echo "Decrement Operation Result: $c <br/>";
	
####################### COMPARISON OPERATORS ##########################
print '<h3>Comparison Operators Examples</h3>';
	$a = 12;
	$b = 25;
	if( $a == $b ) {
		echo "Comparison ex. 1: a is equal to b<br/>";
	}else {
		echo "Comparison ex. 1: a is not equal to b<br/>";
	}
      
	if( $a > $b ) {
		echo "Comparison ex. 2: a is greater than  b<br/>";
	}else {
		echo "Comparison ex. 2: a is not greater than b<br/>";
	}
      
	if( $a < $b ) {
		echo "Comparison ex. 3: a is less than  b<br/>";
	}else {
		echo "Comparison ex. 3: a is not less than b<br/>";
	}
      
	if( $a != $b ) {
		echo "Comparison ex. 4: a is not equal to b<br/>";
	}else {
		echo "Comparison ex. 4: a is equal to b<br/>";
	}
      
	if( $a >= $b ) {
		echo "Comparison ex. 5: a is either greater than or equal to b<br/>";
	}else {
		echo "Comparison ex. 5: a is neither greater than nor equal to b<br/>";
	}
      
	if( $a <= $b ) {
		echo "Comparison ex. 6: a is either less than or equal to b<br/>";
	}else {
		echo "Comparison ex. 6: a is neither less than nor equal to b<br/>";
	}

####################### LOGICAL OPERATORS ##########################
print '<h3>Logical Operators Examples</h3>';
	$a = 28;
	$b = 0;
         
	if( $a && $b ) {
		echo "Logical ex. 1: Both a and b are true<br/>";
	}else{
		echo "Logical ex. 1: Either a or b is false<br/>";
	}
         
	if( $a and $b ) {
		echo "Logical ex. 2: Both a and b are true<br/>";
	}else{
		echo "Logical ex. 2: Either a or b is false<br/>";
	}
         
	if( $a || $b ) {
		echo "Logical ex. 3: Either a or b is true<br/>";
	}else{
		echo "Logical ex. 3: Both a and b are false<br/>";
	}
         
	if( $a or $b ) {
		echo "Logical ex. 4: Either a or b is true<br/>";
	}else {
		echo "Logical ex. 4: Both a and b are false<br/>";
	}
         
	$a = 10;
	$b = 20;
         
	if( $a ) {
		echo "Logical ex. 5: a is true <br/>";
	}else {
		echo "Logical ex. 5: a  is false<br/>";
	}
         
	if( $b ) {
		echo "Logical ex. 6: b is true <br/>";
	}else {
		echo "Logical ex. 6: b  is false<br/>";
	}
         
	if( !$a ) {
		echo "Logical ex. 7: a is true <br/>";
	}else {
		echo "Logical ex. 7: a  is false<br/>";
	}
         
	if( !$b ) {
		echo "Logical ex. 8: b is true <br/>";
	}else {
		echo "Logical ex. 8: b  is false<br/>";
	}
####################### ASSIGNMENT OPERATORS ##########################
print '</div>';
print '<div class="col-lg-6">';
print '<h3>Assingment Operators Examples</h3>';
	$a = 34;
	$b = 12;
         
	$c = $a + $b;  
	echo "Addtion Operation Result: $c <br/>";
         
	$c += $a;  
	echo "Add AND Assigment Operation Result: $c <br/>";
         
	$c -= $a;
	echo "Subtract AND Assignment Operation Result: $c <br/>";
         
	$c *= $a; 
	echo "Multiply AND Assignment Operation Result: $c <br/>";
         
	$c /= $a; 
	echo "Division AND Assignment Operation Result: $c <br/>";
         
	$c %= $a;
	echo "Modulus AND Assignment Operation Result: $c <br/>";



####################### CONDITIONAL OPERATORS ##########################
	print '<h3>Conditional Operators Examples</h3>';
  	$a = 10;
	$b = 20;
         
	/* If condition is true then assign a to result otheriwse b */
	$result = ($a > $b ) ? $a :$b;
	
	echo "Conditional ex. 1: Value of result is $result<br/>";
         
	/* If condition is true then assign a to result otheriwse b */
	$result = ($a < $b ) ? $a :$b;
         
	echo "Conditional ex. 2: Value of result is $result<br/>";
?>
</div>
<?php
}
require_once ('../inc/footer.php');
?>
